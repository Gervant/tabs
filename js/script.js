const tabsList = document.querySelectorAll('.tabs-title')
const textList = document.querySelectorAll('.text-list')

tabsList.forEach(function (item){
    
    item.addEventListener('click', function (){

        let currentTab = item;
        let dataTab = currentTab.getAttribute('data-tab')
        let currentText = document.querySelector(dataTab)

        textList.forEach(function (item){
            item.classList.remove('active-text')
        })

        tabsList.forEach(function (item){
            item.classList.remove('active')
        })

        currentTab.classList.add('active')
        currentText.classList.add('active-text')

    })
})
